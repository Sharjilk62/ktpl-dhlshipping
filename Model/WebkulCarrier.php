<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpDHLShipping
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Ktpl\Dhlshipping\Model;

use Magento\Catalog\Model\Product\Type;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Framework\Xml\Security;
use Magento\Framework\Session\SessionManager;
use Magento\Shipping\Model\Shipping\LabelGenerator;
use Webkul\MarketplaceBaseShipping\Model\ShippingSettingRepository;
use Webkul\MarketplaceBaseShipping\Model\Carrier\AbstractCarrierOnline;
use Magento\Dhl\Model\Carrier as DhlCarrier;
use Magento\Dhl\Model\Validator\XmlValidator;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Marketplace DHL shipping.
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class WebkulCarrier extends \Webkul\MpDHLShipping\Model\Carrier
{
    /**
     * Code of the carrier.
     *
     * @var string
     */
    const CODE = 'mpdhl';
    /**
     * Code of the carrier.
     *
     * @var string
     */
    protected $_code = self::CODE;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager = null;
    /**
     * [$_coreSession description].
     *
     * @var [type]
     */
    protected $_coreSession;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * @var [type]
     */
    protected $_region;
    /**
     * @var LabelGenerator
     */
    protected $_labelGenerator;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    /**
     * Rate result data.
     *
     * @var Result|null
     */
    protected $_result = null;

    protected $_customerFactory;

    protected $requestParam;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface             $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory     $rateErrorFactory
     * @param \Psr\Log\LoggerInterface                                       $logger
     * @param Security                                                       $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory               $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory                     $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory    $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory                 $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory           $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory          $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory                         $regionFactory
     * @param \Magento\Directory\Model\CountryFactory                        $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory                       $currencyFactory
     * @param \Magento\Directory\Helper\Data                                 $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface           $stockRegistry
     * @param \Magento\Store\Model\StoreManagerInterface                     $storeManager
     * @param \Magento\Framework\Module\Dir\Reader                           $configReader
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Framework\ObjectManagerInterface                      $objectManager
     * @param SessionManager                                                 $coreSession
     * @param \Magento\Customer\Model\Session                                $customerSession
     * @param LabelGenerator                                                 $labelGenerator
     * @param array                                                          $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Webkul\Marketplace\Helper\Orders $marketplaceOrderHelper,
        \Webkul\Marketplace\Model\ProductFactory $marketplaceProductFactory,
        \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory,
        ShippingSettingRepository $shippingSettingRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Shipping\Helper\Carrier $carrierHelper,
        \Magento\Quote\Model\Quote\Item\OptionFactory $quoteOptionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\RequestInterface $requestInterface,
        SessionManager $coreSession,
        \Magento\Customer\Model\Session $customerSession,
        LabelGenerator $labelGenerator,
        DhlCarrier $dhlCarrier,
        \Magento\Framework\App\Request\Http $requestParam,
        \Magento\Framework\Stdlib\DateTime\DateTime $coreDate,
        \Magento\Dhl\Model\Validator\XmlValidator $xmlValidator = null,
        \Magento\Framework\App\State $state,
        array $data = []
    ) {
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $marketplaceOrderHelper,
            $marketplaceProductFactory,
            $saleslistFactory,
            $shippingSettingRepository,
            $productFactory,
            $addressFactory,
            $customerFactory,
            $carrierHelper,
            $quoteOptionFactory,
            $storeManager,
            $string,
            $httpClientFactory,
            $productCollectionFactory,
            $objectManager,
            $requestInterface,
            $coreSession,
            $customerSession,
            $labelGenerator,
            $dhlCarrier,
            $requestParam,
            $coreDate,
            $xmlValidator,
            $state,
            $data
        );
        $this->logger = $logger;
        $this->_objectManager = $objectManager;
        $this->dhlCarrier = $dhlCarrier;
        $this->_coreDate = $coreDate;
        $this->string = $string;
        $this->state = $state;
        $this->xmlValidator = $xmlValidator
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(XmlValidator::class);
    }

    /**
     * Collect and get rates.
     *
     * @param RateRequest $request
     *
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Error|bool|Result
     */
    public function collectRates(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        if (!$this->canCollectRates()
            || $this->isMultiShippingActive()
        ) {
            return false;
        }
        $this->setRequest($request);
        return $this->getShippingPricedetail($this->_rawRequest);
    }


    protected function _getGatewayUrl()
    {
        if ($this->getConfigData('sandbox_mode')) {
            return $this->getConfigData('sandbox_gateway_url');
        } else {
            return $this->getConfigData('production_gateway_url');
        }
    }

    /**
     * Prepare and set request to this instance.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setRequest(RateRequest $request)
    {
        parent::setRequest($request);
        $destCountry = $request->getDestCountryId();
        if ($destCountry == DhlCarrier::USA_COUNTRY_ID
            && ($request->getDestPostcode() == '00912' || $request->getDestRegionCode() == DhlCarrier::PUERTORICO_COUNTRY_ID)
        ) {
            $destCountry = DhlCarrier::PUERTORICO_COUNTRY_ID;
        }
        $request->setDestCountryId($destCountry);

        return $this;
    }
   

    /**
     * set the configuration values.
     *
     * @param RateRequest $request
     */
    public function setConfigData(RateRequest $request)
    {
        $r = $request;

        $r->setDhlAccessId($this->getConfigData('id'));
        $r->setDhlPassword($this->getConfigData('password'));
        $r->setDhlAccountNumber($this->getConfigData('account'));
        $r->setDhlReadyTime($this->getConfigData('ready_time'));

        return $r;
    }
    /**
     * set seller credentials if he/she has.
     *
     * @param RateRequest $request
     * @param int                           $sellerId
     *
     * @return \Magento\Framework\DataObject
     */
    protected function _isSellerHasOwnCredentials(\Magento\Framework\DataObject $request, $sellerId)
    {
        $customer = $this->customerFactory->create()->load($sellerId);
        if (isset($customer['dhl_access_id'])) {
            $request->setDhlAccessId($customer->getDhlAccessId());
        } elseif ($this->getConfigData('id') == '' && !isset($customer['dhl_access_id'])) {
            $request->setDhlAccessId('');
        }
        if (isset($customer['dhl_account_number'])) {
            $request->setDhlAccountNumber($customer->getDhlAccountNumber());
        } elseif ($this->getConfigData('account') == '' && !isset($customer['dhl_account_number'])) {
            $request->setDhlAccountNumber('');
        }
        if (isset($customer['dhl_password'])) {
            $request->setDhlPassword($customer->getDhlPassword());
        } elseif ($this->getConfigData('password') == '' && !isset($customer['dhl_password'])) {
            $request->setDhlPassword('');
        }
        if (isset($customer['dhl_ready_time'])) {
            $request->setDhlReadyTime($customer->getDhlReadyTime());
        } elseif ($this->getConfigData('ready_time') == '' && !isset($customer['dhl_ready_time'])) {
            $request->setDhlReadyTime('');
        }

        return $request;
    }
    /**
     * Add dimension to piece
     *
     * @param \Magento\Shipping\Model\Simplexml\Element $nodePiece
     * @return void
     */
    protected function _addDimension($nodePiece)
    {
        $sizeChecker = (string)$this->getConfigData('size');

        $height = $this->_getDimension((string)$this->getConfigData('height'));
        $depth = $this->_getDimension((string)$this->getConfigData('depth'));
        $width = $this->_getDimension((string)$this->getConfigData('width'));

        if ($sizeChecker && $height && $depth && $width) {
            $nodePiece->addChild('Height', $height);
            $nodePiece->addChild('Depth', $depth);
            $nodePiece->addChild('Width', $width);
        }
    }

    /**
     * Convert item dimension to needed dimension based on config dimension unit of measure
     *
     * @param float $dimension
     * @param string|bool $configWeightUnit
     * @return float
     */
    protected function _getDimension($wkdimension, $wkconfigWeightUnit = false)
    {
        
        if (!$wkconfigWeightUnit) {
            $wkconfigWeightUnit = $this->getCode(
                'dimensions_variables',
                (string)$this->getConfigData('unit_of_measure')
            );
        } else {
            $wkconfigWeightUnit = $this->getCode('dimensions_variables', $wkconfigWeightUnit);
        }

        if ($wkconfigWeightUnit == \Zend_Measure_Weight::POUND) {
            $configDimensionUnit = \Zend_Measure_Length::INCH;
        } else {
            $configDimensionUnit = \Zend_Measure_Length::CENTIMETER;
        }

        $countryDimensionUnit = $this->getCode('dimensions_variables', $this->_getDhlDimensionUnit());

        if ($configDimensionUnit != $countryDimensionUnit) {
            $wkdimension = $this->_carrierHelper->convertMeasureDimension(
                (float)$wkdimension,
                $configDimensionUnit,
                $countryDimensionUnit
            );
        }

        return sprintf('%.3f', $wkdimension);
    }

    /**
     * Get shipping quotes from DHL service
     *
     * @param string $request
     * @return string
     */
    protected function _getQuotesFromServer($request)
    {
        $client = $this->_httpClientFactory->create();
        $client->setUri((string)$this->_getGatewayUrl());
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
        $client->setRawData(utf8_encode($request));

        return $client->request(\Zend_Http_Client::POST)->getBody();
    }
    /**
     * Makes remote request to the carrier and returns a response.
     *
     * @param string $purpose
     *
     * @return mixed
     */
    protected function _createRatesRequest(RateRequest $request, $shipdetail)
    {
        $request->setOrigCountryId($shipdetail['origin_country_id']);

        $this->originCountryId = $shipdetail['origin_country_id'];

        $xmlStr = '<?xml version = "1.0" encoding = "UTF-8"?>'.
            '<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" '.
            'xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" '.
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
            'xsi:schemaLocation="http://www.dhl.com DCT-req.xsd "/>';
        $xml = $this->_xmlElFactory->create(['data' => $xmlStr]);
        
        $nodeGetQuote = $xml->addChild('GetQuote', '', '');
        $nodeRequest = $nodeGetQuote->addChild('Request');

        $nodeServiceHeader = $nodeRequest->addChild('ServiceHeader');
        $nodeServiceHeader->addChild('SiteID', (string) $request->getDhlAccessId());
        $nodeServiceHeader->addChild('Password', (string) $request->getDhlPassword());
        
        $nodeFrom = $nodeGetQuote->addChild('From');
        $nodeFrom->addChild('CountryCode', $shipdetail['origin_country_id']);
        $nodeFrom->addChild('Postalcode', $shipdetail['origin_postcode']);
        $nodeFrom->addChild('City', $shipdetail['origin_city']);

        $nodeBkgDetails = $nodeGetQuote->addChild('BkgDetails');
        $nodeBkgDetails->addChild('PaymentCountryCode', $shipdetail['origin_country_id']);
        $nodeBkgDetails->addChild(
            'Date',
            (new \DateTime())->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT)
        );
        $nodeBkgDetails->addChild('ReadyTime', 'PT'.(int) (string) $request->getDhlReadyTime().'H00M');
        
        $nodeBkgDetails->addChild('DimensionUnit', $this->_getDhlDimensionUnit());
        
        $nodeBkgDetails->addChild('WeightUnit', $this->_getItemWeightUnit());
        
        $nodePieces = $nodeBkgDetails->addChild('Pieces', '', '');
        $nodePiece = $nodePieces->addChild('Piece', '', '');
        $nodePiece->addChild('PieceID', 1);
        
        $this->_addDimension($nodePiece);
        
        $nodePiece->addChild('Weight', $this->_getActualWeight($shipdetail['items_weight']));
        
        $nodeBkgDetails->addChild('PaymentAccountNumber', (string) $request->getDhlAccountNumber());

        $nodeTo = $nodeGetQuote->addChild('To');
        $nodeTo->addChild('CountryCode', $request->getDestCountryId());
        $nodeTo->addChild('Postalcode', $request->getDestPostal());
        $nodeTo->addChild('City', $request->getDestCity());
        
        if ($this->isDutiable($request->getOrigCountryId(), $request->getDestCountryId())) {
            
            // IsDutiable flag and Dutiable node indicates that cargo is not a documentation
            $nodeBkgDetails->addChild('IsDutiable', 'Y');
            $nodeDutiable = $nodeGetQuote->addChild('Dutiable');

            $baseCurrencyCode = $this->storeManager->getStore()->getBaseCurrencyCode();
            $nodeDutiable->addChild('DeclaredCurrency', $baseCurrencyCode);
            $nodeDutiable->addChild('DeclaredValue', sprintf('%.2F', $shipdetail['price']));
        }
        
        return $xml;

    }

    /**
     * Set pick-up date in request XML object
     *
     * @param \SimpleXMLElement $requestXml
     * @param string $date
     * @return \SimpleXMLElement
     */
    protected function _setDhlQuotesRequestXmlDate(\SimpleXMLElement $requestXmlData, $date)
    {
        $requestXmlData->GetQuote->BkgDetails->Date = $date;

        return $requestXmlData;
    }

    /**
     * Build RateV3 request, send it to Dhl gateway and retrieve quotes in XML format.
     *
     * @return Result
     */
    public function getShippingPricedetail(RateRequest $request)
    {
        $this->setConfigData($request);
        $r = $request;
        $submethod = [];
        $shippinginfo = [];
        $totalpric = [];
        $totalPriceArr = [];
        $serviceCodeToActualNameMap = [];
        $costArr = [];
        $debugData = [];
        $price = 0;
        $isStorePickup = false;
        $flag = false;
        $check = false;
        
        foreach ($r->getShippingDetails() as $shipdetail) {
            $priceArr = [];
            $itemPriceDetails = [];
            // $shipdetail['origin_country_id'] = "AE";
            // $shipdetail['origin_postcode'] = "";
            // $shipdetail['origin_city'] = "Dubai";
            $this->_isSellerHasOwnCredentials($request, $shipdetail['seller_id']);
            $responseBody = '';
            try {
                for ($offset = 0; $offset <= DhlCarrier::UNAVAILABLE_DATE_LOOK_FORWARD; ++$offset) {
                    
                    $debugData['try-'.$offset] = [];
                    $debugPoint = &$debugData['try-'.$offset];

                    $requestXml = $this->_createRatesRequest($request, $shipdetail);
                    
                    $date = date(DhlCarrier::REQUEST_DATE_FORMAT, strtotime($this->_getShipDate()." +{$offset} days"));
                    $this->_setDhlQuotesRequestXmlDate($requestXml, $date);
                    
                    $requestData = $requestXml->asXML();
                    $debugPoint['request'] = $requestData;
                    $responseBody = $this->_getCachedQuotes($requestData);

                    $debugPoint['from_cache'] = $responseBody === null;
                    if ($debugPoint['from_cache']) {
                        $responseBody = $this->_getQuotesFromServer($requestData);
                    }
                    $debugPoint['response'] = $responseBody;
                    $bodyXml = $this->_xmlElFactory->create(['data' => $responseBody]);
                    $code = $bodyXml->xpath('//GetQuoteResponse/Note/Condition/ConditionCode');
                    if (isset($code[0]) && (int) $code[0] == DhlCarrier::CONDITION_CODE_SERVICE_DATE_UNAVAILABLE) {
                        $debugPoint['info'] = sprintf(__('DHL service is not available at %s date'), $date);
                    } else {
                        break;
                    }
                    $this->_setCachedQuotes($requestData, $responseBody);
                }
                $this->_debug($debugData);
            } catch (\Exception $e) {
                $this->_errors[$e->getCode()] = $e->getMessage();
            }
            if(isset($debugPoint['request']))
            {
                $this->logger->info($debugPoint['request']);
            }
            if(isset($debugPoint['response']))
            {
                $this->logger->info($debugPoint['response']);
            }
            list($priceArr, $costArr) = $this->_parseRateResponse($responseBody);
            $price = 0;
            $flag = $this->_filterSellerRate($priceArr);
            if ($flag) {
                $debugData['result'] = ['error' => 1];
                if ($this->isMultiShippingActive() ||
                    $this->isStorePickupActive()) {
                    return [];
                } else {
                    return $this->_parseXmlResponse($debugData);
                }
            }
            if ($this->isStorePickupActive()) {
                /*Calculate price itemwise for seller store pickup*/
                $items = explode(',', $shipdetail['item_id']);
                $newShipderails = [];
                $newShipderails['seller_id'] = $shipdetail['seller_id'];
                $newShipderails['origin_country_id'] = $shipdetail['origin_country_id'];
                $newShipderails['origin_postcode'] = $shipdetail['origin_postcode'];
                $newShipderails['origin_city'] = $shipdetail['origin_city'];
                
                if (isset($shipdetail['item_weight_details']) &&
                    isset($shipdetail['item_product_price_details']) &&
                    isset($shipdetail['item_qty_details'])
                    ) {
                    $isStorePickup = true;
                    foreach ($items as $itemId) {
                        $newShipderails['items_weight'] = $shipdetail['item_weight_details'][$itemId];
                        $newShipderails['price'] = $shipdetail['item_product_price_details'][$itemId] * $shipdetail['item_qty_details'][$itemId];
                        $requestData =  $this->_createRatesRequest($newShipderails);
                        $requestData = $requestXml->asXML();
                        $responseBody = $this->_getQuotesFromServer($requestData);
                        list($itemPriceArr, $itemCostArr) = $this->_parseRateResponse($responseBody);
                        $itemPriceDetails[$itemId] = $itemPriceArr;
                    }
                }
            }
            $submethod = [];
            foreach ($priceArr as $index => $price) {
                $submethod[$index] = [
                    'method' => $this->dhlCarrier->getDhlProductTitle($index).' (DHL)',
                    'cost' => $price,
                    'base_amount' => $price,
                    'error' => 0,
                ];
            }
            if (!isset($shipdetail['item_id_details'])) {
                $shipdetail['item_id_details'] = [];
            }
            if (!isset($shipdetail['item_name_details'])) {
                $shipdetail['item_name_details'] = [];
            }
            if (!isset($shipdetail['item_qty_details'])) {
                $shipdetail['item_qty_details'] = [];
            }
            array_push(
                $shippinginfo,
                [
                    'seller_id' => $shipdetail['seller_id'],
                    'methodcode' => $this->_code,
                    'shipping_ammount' => $price,
                    'product_name' => $shipdetail['product_name'],
                    'submethod' => $submethod,
                    'item_ids' => $shipdetail['item_id'],
                    'item_price_details' => $itemPriceDetails,
                    'item_id_details' => $shipdetail['item_id_details'],
                    'item_name_details' => $shipdetail['item_name_details'],
                    'item_qty_details' => $shipdetail['item_qty_details']
                ]
            );
        }
        $totalpric = ['totalprice' => $this->_totalPriceArr, 'costarr' => $this->_costArr];
        $debugData['result'] = $totalpric;
        $result = ['handlingfee' => $totalpric, 'shippinginfo' => $shippinginfo, 'error' => $debugData];
        $shippingAll = $this->_coreSession->getData('shippinginfo');
        $shippingAll = $this->_coreSession->getShippingInfo();
        $shippingAll[$this->_code] = $result['shippinginfo'];
        $this->_coreSession->setShippingInfo($shippingAll);

        if ($this->isMultiShippingActive() || $isStorePickup) {
            return $result;
        } else {
            return $this->_parseXmlResponse($totalpric);
        }
    }

    /**
     * Get the rates from response.
     * @param  xml $response
     * @return array [$priceArr, $costArr];
     */
    public function _parseRateResponse($response)
    {
        $priceArr = [];
        $costArr = [];
        if (strlen(trim($response)) > 0) {
            if (strpos(trim($response), '<?xml') === 0) {
                $xml = $this->parseXml($response, 'Magento\Shipping\Model\Simplexml\Element');
                if (is_object($xml)) {
                    if (isset($xml->GetQuoteResponse->BkgDetails->QtdShp)) {
                        foreach ($xml->GetQuoteResponse->BkgDetails->QtdShp as $quotedShipment) {
                            $dhlProduct = (string) $quotedShipment->GlobalProductCode;
                            $totalEstimate = (float) (string) $quotedShipment->ShippingCharge;
                            $currencyCode = (string) $quotedShipment->CurrencyCode;
                            $baseCurrencyCode = $this->storeManager->getStore()->getBaseCurrencyCode();
                            $dhlProductDescription = $this->dhlCarrier->getDhlProductTitle($dhlProduct);
                            $serviceCodeToActualNameMap[$dhlProduct] = $dhlProductDescription;
                            if ($currencyCode != $baseCurrencyCode) {
                                /* @var $currency \Magento\Directory\Model\Currency */
                                $currency = $this->_currencyFactory->create();
                                $rates = $currency->getCurrencyRates($currencyCode, [$baseCurrencyCode]);
                                if (!empty($rates) && isset($rates[$baseCurrencyCode])) {
                                    // Convert to store display currency using store exchange rate
                                    $totalEstimate = $totalEstimate * $rates[$baseCurrencyCode];
                                } else {
                                    $rates = $currency->getCurrencyRates($baseCurrencyCode, [$currencyCode]);
                                    if (!empty($rates) && isset($rates[$currencyCode])) {
                                        $totalEstimate = $totalEstimate / $rates[$currencyCode];
                                    }
                                    if (!isset($rates[$currencyCode]) || !$totalEstimate) {
                                        $totalEstimate = false;
                                        $this->_errors[] = __(
                                            'We had to skip DHL method %1 because we couldn\'t 
                                            find exchange rate %2 (Base Currency).',
                                            $currencyCode,
                                            $baseCurrencyCode
                                        );
                                    }
                                }
                                if (!isset($quotedShipment->CurrencyCode)) {
                                    $totalEstimate = (float) (string) $quotedShipment->ShippingCharge;
                                }
                            }
                            
                            if ($this->getConfigData('content_type') == "D") {
                                $allowedMethods = $this->getConfigData('doc_methods');
                            }
                            if ($this->getConfigData('content_type') == "N") {
                                $allowedMethods = $this->getConfigData('nondoc_methods');
                            }
                           if (array_key_exists(
                                (string) $quotedShipment->GlobalProductCode,
                                $this->dhlCarrier->getAllowedMethods()
                                )
                            ) {
                                if ($totalEstimate) {
                                    $costArr[$dhlProduct] = $totalEstimate;
                                    $priceArr[$dhlProduct] = $this->dhlCarrier->getMethodPrice($totalEstimate, $dhlProduct);
                                }
                            }
                        }
                        asort($priceArr);
                        return [$priceArr, $costArr];
                    } else {
                        return [$priceArr, $costArr];
                    }
                }
            }
        }
    }
    /**
     * Parse calculated rates.
     *
     * @param string $response
     *
     * @return Result
     */
    protected function _parseXmlResponse($response)
    {
        $result = $this->_rateFactory->create();
        if (isset($response['result']['error']) && $response['result']['error']) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier('mpdhl');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        } else {
            $totalPriceArr = $response['totalprice'];
            $costArr = $response['costarr'];
            foreach ($totalPriceArr as $method => $price) {
                $rate = $this->_rateMethodFactory->create();
                $rate->setCarrier('mpdhl');
                $rate->setCarrierTitle($this->getConfigData('title'));
                $rate->setMethod($method);
                $rate->setMethodTitle($this->dhlCarrier->getDhlProductTitle($method));
                $rate->setCost($price);
                $rate->setPrice($price);
                $result->append($rate);
            }
        }

        return $result;
    }
    /**
     * Prepare shipping label data.
     *
     * @param \SimpleXMLElement $xml
     *
     * @return \Magento\Framework\DataObject
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareShippingLabelContent(\SimpleXMLElement $xml)
    {
        $result = new \Magento\Framework\DataObject();
        try {
            if (!isset($xml->AirwayBillNumber) || !isset($xml->LabelImage->OutputImage)) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Unable to retrieve shipping label'));
            }
            $result->setTrackingNumber((string) $xml->AirwayBillNumber);
            $labelContent = (string)$xml->LabelImage->OutputImage;
            $result->setShippingLabelContent(base64_decode($labelContent));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }

        return $result;
    }
    /**
     * Do request to shipment
     *
     * @param \Magento\Shipping\Model\Shipment\Request $request
     * @return array|\Magento\Framework\DataObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function requestToShipment($request)
    {
        $packages = $request->getPackages();
      
        $this->setRawRequest($request);
        $orderId = $this->requestParam->getParam('order_id');
        $request->setOrderId($orderId);

        if (!is_array($packages) || !$packages) {
            throw new \Magento\Framework\Exception\LocalizedException(__('No packages for request'));
        }
        
        if (!$this->validPackages($packages)) {
            $errorMessage = "Please check the dimensions of the package entered.";
            throw new \Magento\Framework\Exception\LocalizedException(__($errorMessage));
        }
        $result = $this->_doShipmentRequest($request);

        $response = new \Magento\Framework\DataObject(
            [
                'info' => [
                    [
                        'tracking_number' => $result->getTrackingNumber(),
                        'label_content' => $result->getShippingLabelContent(),
                    ],
                ],
            ]
        );

        $request->setMasterTrackingId($result->getTrackingNumber());

        return $response;
    }

    /**
     * Map request to shipment
     *
     * @param \Magento\Framework\DataObject $request
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _mapRequestToShipment(\Magento\Framework\DataObject $rowRequest)
    {
        $rowRequest->setOrigCountryId($rowRequest->getShipperAddressCountryCode());
        $this->setRawRequest($rowRequest);
        $customsValue = 0;
        $packageWeight = 0;
        $totalValue = 0;
        $packages = $rowRequest->getPackages();
        foreach ($packages as &$piece) {
            $params = $piece['params'];
            if ($params['width'] || $params['length'] || $params['height']) {
                $minValue = $params['dimension_units'] == "CENTIMETER" ? DhlCarrier::DIMENSION_MIN_CM : DhlCarrier::DIMENSION_MIN_IN;
                if ($params['width'] < $minValue || $params['length'] < $minValue || $params['height'] < $minValue) {
                    $message = __('Height, width and length should be equal or greater than %1', $minValue);
                    throw new \Magento\Framework\Exception\LocalizedException($message);
                }
            }

            $weightUnit = $piece['params']['weight_units'];
            $piece['params']['height'] = $this->_getDimension($piece['params']['height'], $weightUnit);
            $piece['params']['length'] = $this->_getDimension($piece['params']['length'], $weightUnit);
            $piece['params']['width'] = $this->_getDimension($piece['params']['width'], $weightUnit);
            $piece['params']['dimension_units'] = $this->_getDhlDimensionUnit();
            $piece['params']['weight'] = $this->_getDhlWeight($piece['params']['weight'], false, $weightUnit);
            $piece['params']['weight_units'] = $this->_getItemWeightUnit();

            $customsValue += $piece['params']['customs_value'];
            $packageWeight += $piece['params']['weight'];
            foreach ($piece['items'] as $value) {
                $totalValue += $value['price'];
            }
            
        }
        
        $rowRequest->setPackages($packages)
            ->setPackageWeight($packageWeight)
            ->setPackageValue($customsValue)
            ->setTotalValue($totalValue)
            ->setValueWithDiscount($customsValue)
            ->setPackageCustomsValue($customsValue)
            ->setFreeMethodWeight(0);
    }

    /**
     * Do shipment request to carrier web service,.
     *
     * @param \Magento\Framework\DataObject $request
     *
     * @return \Magento\Framework\DataObject
     */
    public function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        $this->_prepareShipmentRequest($request);
        $this->_mapRequestToShipment($request);
        $this->_request = $request;
        $this->dhlCarrier->setRequest($request);
        $this->setShipemntRequest($request);
        //if ($this->_isShippingMethod()) {
        
        $response = $this->_createShipmentRequest();
        $result = new \Magento\Framework\DataObject();
        
        $this->xmlValidator->validate($response);
        $xml = simplexml_load_string($response);
        
        if (is_object($xml)) {
            if (in_array($xml->getName(), ['ErrorResponse', 'ShipmentValidateErrorResponse'])
            || isset($xml->GetQuoteResponse->Note->Condition)
            ) {
                $code = null;
                $data = null;
                if (isset($xml->Response->Status->Condition)) {
                    $nodeCondition = $xml->Response->Status->Condition;
                }
                foreach ($nodeCondition as $condition) {
                    $code = isset($condition->ConditionCode) ? (string) $condition->ConditionCode : 0;
                    $data = isset($condition->ConditionData) ? (string) $condition->ConditionData : '';
                    if (!empty($code) && !empty($data)) {
                        break;
                    }
                }
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Error #%1 : %2', trim($code), trim($data))
                );
            } elseif (isset($xml->AirwayBillNumber)) {
                $labelResponse = $this->_prepareShippingLabelContent($xml);
                $result->setShippingLabelContent($labelResponse->getShippingLabelContent());
                $result->setTrackingNumber($labelResponse->getTrackingNumber());

                $shipmentData = [
                    'api_name' => 'DHL',
                    'tracking_number' => $labelResponse->getTrackingNumber()
                ];
                $this->_customerSession->setData('shipment_data', $shipmentData);
            }
        }
        return $result;
        //}
    }
    /**
     * Make DHL Shipment Request
     * @return string xml
     */
    protected function _createShipmentRequest()
    {
        $request = $this->_request;

        $originRegionCode = $this->getCountryParams(
            $this->_scopeConfig->getValue(
                "shipping/origin/country_id",
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $this->getStore()
            )
        )->getRegion();
        
        if ($this->state->getAreaCode() == "frontend") {
            $customerId = $this->_customerSession->getCustomerId();
            $originRegionCode = $this->getCountryParams(
                $this->shippingSettingRepository->getBySellerId($customerId)->getCountryId()
            )->getRegion();
        }
        
        if (!$originRegionCode) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Wrong Region'));
        }

        if ($originRegionCode == 'AM') {
            $originRegionCode = '';
        }

        $xmlStr = '<?xml version="1.0" encoding="UTF-8"?>' .
            '<req:ShipmentValidateRequest' .
            $originRegionCode .
            ' xmlns:req="http://www.dhl.com"' .
            ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' .
            ' xsi:schemaLocation="http://www.dhl.com ship-val-req' .
            ($originRegionCode ? '_' .
                $originRegionCode : '') .
            '.xsd" />';
        $xml = $this->_xmlElFactory->create(['data' => $xmlStr]);

        $nodeRequest = $xml->addChild('Request', '', '');
        $nodeServiceHeader = $nodeRequest->addChild('ServiceHeader');
        $nodeServiceHeader->addChild('SiteID', (string) $request->getAccessId());
        $nodeServiceHeader->addChild('Password', (string) $request->getPassword());

        if (!$originRegionCode) {
            $xml->addChild('RequestedPickupTime', 'N', '');
        }
        $xml->addChild('NewShipper', 'N', '');
        $xml->addChild('LanguageCode', 'EN', '');
        $xml->addChild('PiecesEnabled', 'Y', '');

        /* Billing */
        $nodeBilling = $xml->addChild('Billing', '', '');
        $nodeBilling->addChild('ShipperAccountNumber', (string) $request->getAccountNumber());
        /*
         * Method of Payment:
         * S (Shipper)
         * R (Receiver)
         * T (Third Party)
         */
        $nodeBilling->addChild('ShippingPaymentType', 'S');

        /*
         * Shipment bill to account – required if Shipping PaymentType is other than 'S'
         */
        $nodeBilling->addChild('BillingAccountNumber', (string) $request->getAccountNumber());
        $nodeBilling->addChild('DutyPaymentType', 'S');
        $nodeBilling->addChild('DutyAccountNumber', (string) $request->getAccountNumber());

        /* Receiver */
        $nodeConsignee = $xml->addChild('Consignee', '', '');

        $companyName = $request->getRecipientContactCompanyName() ? $request
            ->getRecipientContactCompanyName() : $request
            ->getRecipientContactPersonName();

        $nodeConsignee->addChild('CompanyName', substr($companyName, 0, 35));

        $address = $request->getRecipientAddressStreet1().' '.$request->getRecipientAddressStreet2();
        $address = $this->string->split($address, 35, false, true);
        if (is_array($address)) {
            foreach ($address as $addressLine) {
                $nodeConsignee->addChild('AddressLine', $addressLine);
            }
        } else {
            $nodeConsignee->addChild('AddressLine', $address);
        }

        $nodeConsignee->addChild('City', $request->getRecipientAddressCity());
        $nodeConsignee->addChild('Division', $request->getRecipientAddressStateOrProvinceCode());
        $nodeConsignee->addChild('PostalCode', $request->getRecipientAddressPostalCode());
        $nodeConsignee->addChild('CountryCode', $request->getRecipientAddressCountryCode());
        $nodeConsignee->addChild(
            'CountryName',
            $this->getCountryParams($request->getRecipientAddressCountryCode())->getName()
        );
        $nodeContact = $nodeConsignee->addChild('Contact');
        $nodeContact->addChild('PersonName', substr($request->getRecipientContactPersonName(), 0, 34));
        $nodeContact->addChild('PhoneNumber', substr($request->getRecipientContactPhoneNumber(), 0, 24));

        /*
         * Commodity
         * The CommodityCode element contains commodity code for shipment contents. Its
         * value should lie in between 1 to 9999.This field is mandatory.
         */
        $nodeCommodity = $xml->addChild('Commodity', '', '');
        $nodeCommodity->addChild('CommodityCode', '1');

        /* Dutiable */
        if ($this->isDutiable(
            $request->getShipperAddressCountryCode(),
            $request->getRecipientAddressCountryCode()
        )) {
            $nodeDutiable = $xml->addChild('Dutiable', '', '');
            $nodeDutiable->addChild(
                'DeclaredValue',
                sprintf('%.2F', $request->getTotalValue())
            );
            $baseCurrencyCode = $this->storeManager->getStore()->getBaseCurrencyCode();
            $nodeDutiable->addChild('DeclaredCurrency', $baseCurrencyCode);
        }

        /*
         * Reference
         * This element identifies the reference information. It is an optional field in the
         * shipment validation request. Only the first reference will be taken currently.
         */
        $nodeReference = $xml->addChild('Reference', '', '');
        $nodeReference->addChild('ReferenceID', 'shipment reference');
        $nodeReference->addChild('ReferenceType', 'St');

        /* Shipment Details */
         $this->_setItemsDetails($xml, $request, $originRegionCode);

        /* Shipper */
        $nodeShipper = $xml->addChild('Shipper', '', '');
        $nodeShipper->addChild('ShipperID', (string) $request->getAccountNumber());
        $shipperCompanyName = $request->getShipperContactCompanyName() ? $request
            ->getShipperContactCompanyName() : $request
            ->getShipperContactPersonName();
        $nodeShipper->addChild('CompanyName', $shipperCompanyName);
        $nodeShipper->addChild('RegisteredAccount', (string) $request->getAccountNumber());

        $address = $request->getShipperAddressStreet1().' '.$request->getShipperAddressStreet2();
        $address = $this->string->split($address, 35, false, true);
        if (is_array($address)) {
            foreach ($address as $addressLine) {
                $nodeShipper->addChild('AddressLine', $addressLine);
            }
        } else {
            $nodeShipper->addChild('AddressLine', $address);
        }

        $nodeShipper->addChild('City', $request->getShipperAddressCity());
        $nodeShipper->addChild('Division', $request->getShipperAddressStateOrProvinceCode());
        $nodeShipper->addChild('PostalCode', '92110');
        $nodeShipper->addChild('CountryCode', $request->getShipperAddressCountryCode());
        $nodeShipper->addChild(
            'CountryName',
            $this->getCountryParams($request->getShipperAddressCountryCode())->getName()
        );
        $nodeContact = $nodeShipper->addChild('Contact', '', '');
        $nodeContact->addChild('PersonName', substr($request->getShipperContactPersonName(), 0, 34));
        $nodeContact->addChild('PhoneNumber', substr($request->getShipperContactPhoneNumber(), 0, 24));
        
        $xml->addChild('LabelImageFormat', 'PDF', '');

        // If seller and Admin want to display custom logo.
        list($logoPath, $extension, $size) = $this->_getLogoImage();
        
        if ($logoPath != '') {
            $logo = base64_encode(file_get_contents($logoPath));
            if ((int) $size > 1048576) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Store Logo image size too large, allowed image size is 1 MB.'));
            } else {
                $nodeLabel = $xml->addChild('Label', '', '');
                $nodeLabel->addChild('Logo', 'Y');
                $customerLogo = $nodeLabel->addChild('CustomerLogo', '', '');
                $customerLogo->addChild('LogoImage', $logo);
                $customerLogo->addChild('LogoImageFormat', strtoupper($extension));
                $nodeLabel->addChild('Resolution', '200');
            }
        }
        $requestData = $xml->asXML();
        
        if (!$requestData && !mb_detect_encoding($requestData) == 'UTF-8') {
            $requestData = utf8_encode($requestData);
        }
        $debugData = ['request' => $requestData];
        try {
            $client = $this->_httpClientFactory->create();
            $client->setUri((string) $this->_getGatewayUrl());
            $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
            $client->setRawData($requestData);
            $responseBody = $client->request(\Magento\Framework\HTTP\ZendClient::POST)->getBody();
            $responseBody = utf8_decode($responseBody);
            $debugData['result'] = $responseBody;
        } catch (\Exception $e) {
            $this->_errors[$e->getCode()] = $e->getMessage();
            $responseBody = '';
        }
        
        $this->_debug($debugData);

        return $responseBody;
    }
    /**
     * If admin allowed to display logo on label
     * @return array
     */
    protected function _getLogoImage()
    {
        $logoPath = '';
        $extension = 'JPG';
        $fileName = "";
        $flag = false;
        $size = '';
        $marketplaceHelper = $this->_objectManager->create('Webkul\Marketplace\Helper\Data');
        if ($this->getConfigData('dhl_logo_display')) {
            $filesystem = $this->_objectManager->get(\Magento\Framework\Filesystem::class);
            $directory = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
            $fileName = 'dhl/logo/' . ltrim($this->getConfigData('dhl_logo'), '/');
            $logoPath = $directory->getAbsolutePath($fileName);
            $extensionArray = explode('.', $logoPath);
            $extension = end($extensionArray);
            $stat = $directory->stat($fileName);
            $size = $stat['size'];
            $flag = true;
        }
        $filesystem = $this->_objectManager->get(\Magento\Framework\Filesystem::class);
        $directory = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
        if ($this->getConfigData('allow_seller_logo')) {
            $displaySellerLogo = $this->_customerSession->getCustomer()->getDhlLogo();
            if ($displaySellerLogo) {
                $seller = $marketplaceHelper->getSeller();
                $fileName = 'avatar/' . ltrim($seller['logo_pic'], '/');
                $logoPath = $directory->getAbsolutePath($fileName);
                $extensionArray = explode('.', $logoPath);
                $extension = end($extensionArray);
                $stat = $directory->stat($fileName);
                $size = $stat['size'];
                $flag = true;
            } elseif ($this->getConfigData('dhl_logo_display')) {
                $fileName = 'dhl/logo/' . ltrim($this->getConfigData('dhl_logo'), '/');
                $logoPath = $directory->getAbsolutePath($fileName);
                $extensionArray = explode('.', $logoPath);
                $extension = end($extensionArray);
                $stat = $directory->stat($fileName);
                $size = $stat['size'];
                $flag = true;
            }
        }
        if (!$directory->isFile($fileName) && $flag) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Custom logo for shipping label not found, please upload PNG, GIF, JPEG, JPG image type.')
            );
        }

        return [$logoPath, $extension, $size];
    }
    /**
     * Add seller items details in request.
     * @param string                          $xml
     * @param  \Magento\Framework\DataObject $request
     * @param string                         $originRegionCode
     */
    public function _setItemsDetails($xml, $request, $originRegionCode)
    {
        $nodeShipmentDetails = $xml->addChild('ShipmentDetails', '', '');
        $nodeShipmentDetails->addChild('NumberOfPieces', count($request->getPackages()));

        if ($originRegionCode) {
            $nodeShipmentDetails->addChild(
                'CurrencyCode',
                $this->storeManager->getWebsite($this->_request->getWebsiteId())->getBaseCurrencyCode()
            );
        }

        $nodePieces = $nodeShipmentDetails->addChild('Pieces', '', '');


        $i = 0;
        $weight = 0;
        $salesListModel = [];
        $itemsDesc = [];
        $itemsQty = 0;
        $i = 0;
        foreach ($request->getPackages() as $package) {
            $nodePiece = $nodePieces->addChild('Piece', '', '');
            $packageType = 'EE';
            if ($package['params']['container'] == DhlCarrier::DHL_CONTENT_TYPE_NON_DOC) {
                $packageType = 'CP';
            }
            $nodePiece->addChild('PieceID', ++$i);
            $nodePiece->addChild('PackageType', $packageType);
            $nodePiece->addChild('Weight', sprintf('%.1f', $package['params']['weight']));
            $params = $package['params'];
            if ($params['width'] && $params['length'] && $params['height']) {
                if (!$originRegionCode) {
                    $nodePiece->addChild('Width', round($params['width']));
                    $nodePiece->addChild('Height', round($params['height']));
                    $nodePiece->addChild('Depth', round($params['length']));
                } else {
                    $nodePiece->addChild('Depth', round($params['length']));
                    $nodePiece->addChild('Width', round($params['width']));
                    $nodePiece->addChild('Height', round($params['height']));
                }
            }
            $content = [];
            foreach ($package['items'] as $item) {
                $content[] = $item['name'];
            }
            $nodePiece->addChild('PieceContents', substr(implode(',', $content), 0, 34));
        }

        if (!$originRegionCode) {
            $nodeShipmentDetails->addChild('Weight', sprintf('%.1f', $request->getPackageWeight()));
            $nodeShipmentDetails->addChild('WeightUnit', substr($this->_getItemWeightUnit(), 0, 1));
            $nodeShipmentDetails->addChild('GlobalProductCode', $request->getShippingMethod());
            $nodeShipmentDetails->addChild('LocalProductCode', $request->getShippingMethod());
            $nodeShipmentDetails->addChild('Date', $this->_coreDate->date('Y-m-d'));
            $nodeShipmentDetails->addChild('Contents', 'DHL Parcel');
            /**
             * The DoorTo Element defines the type of delivery service that applies to the shipment.
             * The valid values are DD (Door to Door), DA (Door to Airport) , AA and DC (Door to
             * Door non-compliant)
             */
            $nodeShipmentDetails->addChild('DoorTo', 'DD');
            $nodeShipmentDetails->addChild('DimensionUnit', substr($this->_getDhlDimensionUnit(), 0, 1));
            if ($package['params']['container'] == DhlCarrier::DHL_CONTENT_TYPE_NON_DOC) {
                $packageType = 'CP';
            }
            $nodeShipmentDetails->addChild('PackageType', $packageType);
            if ($this->isDutiable($request->getOrigCountryId(), $request->getDestCountryId())) {
                $nodeShipmentDetails->addChild('IsDutiable', 'Y');
            }
            $nodeShipmentDetails->addChild(
                'CurrencyCode',
                $this->storeManager->getWebsite($this->_request->getWebsiteId())->getBaseCurrencyCode()
            );
        } else {
            if ($this->getConfigData('content_type') == DhlCarrier::DHL_CONTENT_TYPE_NON_DOC) {
                $packageType = 'CP';
            }
            $nodeShipmentDetails->addChild('PackageType', $packageType);
            $nodeShipmentDetails->addChild('Weight', sprintf('%.3f', $request->getPackageWeight()));
            $nodeShipmentDetails->addChild('DimensionUnit', substr($this->_getDhlDimensionUnit(), 0, 1));
            $nodeShipmentDetails->addChild('WeightUnit', substr($this->_getItemWeightUnit(), 0, 1));
            $nodeShipmentDetails->addChild('GlobalProductCode', $request->getShippingMethod());
            $nodeShipmentDetails->addChild('LocalProductCode', $request->getShippingMethod());

            /*
             * The DoorTo Element defines the type of delivery service that applies to the shipment.
             * The valid values are DD (Door to Door), DA (Door to Airport) , AA and DC (Door to
             * Door non-compliant)
             */
            $nodeShipmentDetails->addChild('DoorTo', 'DD');
            $nodeShipmentDetails->addChild('Date', $this->_coreDate->date('Y-m-d'));
            $nodeShipmentDetails->addChild('Contents', 'DHL Parcel TEST');
        }
    }
    /**
     * set recipent and seller data in object.
     */
    public function setShipemntRequest(\Magento\Framework\DataObject $request)
    {
        $customerId = $this->_customerSession->getCustomerId();
        //set default credentials
        $request->setAccessId($this->getConfigData('id'));
        $request->setPassword($this->getConfigData('password'));
        $request->setAccountNumber($this->getConfigData('account'));
        $request->setReadyTime($this->getConfigData('ready_time'));

        //set seller credentials
        $this->_isSellerHasOwnCredentials($request, $customerId);

        $this->setRawRequest($request);

        return $this;
    }

    /**
     * Get tracking
     *
     * @param string|string[] $trackings
     * @return Result|null
     */
    public function getTracking($trackings)
    {
        return $this->dhlCarrier->getTracking($trackings);
    }

    /**
     * get product weight
     * @param  object $item
     * @return int
     */
    protected function _getItemWeight($item)
    {
        $weight = 0;
        if ($item->getHasChildren() && $item->isShipSeparately()) {
            foreach ($item->getChildren() as $child) {
                if (!$child->getProduct()->isVirtual()) {
                    $weight = $item->getWeight() * $item->getQty();
                }
            }
        } else {
            $weight = $item->getWeight() * $item->getQty();
        }

        return $weight;
    }

    /**
     * Convert item weight to needed weight based on config weight unit dimensions
     *
     * @param float $weight
     * @param bool $maxWeight
     * @param string|bool $configWeightUnit
     * @return float
     */
    protected function _getActualWeight($weight)
    {
        $configWeightUnit = $this->getCode(
            'dimensions_variables',
            (string)$this->getConfigData('unit_of_measure')
        );
        $countryWeightUnit = $this->getCode('dimensions_variables', $this->_getItemWeightUnit());

        if ($configWeightUnit != $countryWeightUnit) {
            $weight = $this->_carrierHelper->convertMeasureWeight(
                round($weight, 3),
                $configWeightUnit,
                $countryWeightUnit
            );
        }
        
        return round($weight, 3);
    }

    /**
     * Convert item weight to needed weight based on config weight unit dimensions
     *
     * @param float $weight
     * @param bool $maxWeight
     * @param string|bool $configWeightUnit
     * @return float
     */
    protected function _getDhlWeight($weight, $maxWeight = false, $configWeightUnit = false)
    {
        if ($maxWeight) {
            $configWeightUnit = \Zend_Measure_Weight::KILOGRAM;
        } elseif ($configWeightUnit) {
            $configWeightUnit = $this->getCode('dimensions_variables', $configWeightUnit);
        } else {
            $configWeightUnit = $this->getCode(
                'dimensions_variables',
                (string)$this->getConfigData('unit_of_measure')
            );
        }

        $countryWeightUnit = $this->getCode('dimensions_variables', $this->_getItemWeightUnit());

        if ($configWeightUnit != $countryWeightUnit) {
            $weight = $this->_carrierHelper->convertMeasureWeight(
                (float)$weight,
                $configWeightUnit,
                $countryWeightUnit
            );
        }

        return sprintf('%.3f', $weight);
    }

    /**
     * Returns dimension unit (cm or inch)
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getDhlDimensionUnit()
    {
        $measureUnit = $this->getCountryParams($this->originCountryId)->getMeasureUnit();
        if (empty($measureUnit)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("Cannot identify measure unit for %1", $this->originCountryId)
            );
        }

        return $measureUnit;
    }

    /**
     * Returns weight unit (kg or pound)
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getItemWeightUnit()
    {
        $weightUnit = $this->getCountryParams($this->originCountryId)->getWeightUnit();
        if (empty($weightUnit)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("Cannot identify weight unit for %1", $this->originCountryId)
            );
        }

        return $weightUnit;
    }

    /**
     * @param string $origCountryId
     * @param string $destCountryId
     *
     * @return bool
     */
    protected function isDutiable($origCountryId, $destCountryId)
    {
        return false;
        $isDomestic = $this->_checkDomesticStatus($origCountryId, $destCountryId);
        return
            DhlCarrier::DHL_CONTENT_TYPE_NON_DOC == $this->getConfigData('content_type')
            && !$isDomestic;
    }

    /**
     * Return container types of carrier
     *
     * @param \Magento\Framework\DataObject|null $params
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getContainerTypes(\Magento\Framework\DataObject $params = null)
    {
        return [
            DhlCarrier::DHL_CONTENT_TYPE_DOC => __('Documents'),
            DhlCarrier::DHL_CONTENT_TYPE_NON_DOC => __('Non Documents')
        ];
    }

    /**
     * load model
     * @param  int $id
     * @param  string $model
     * @return object
     */
    protected function _loadModel($id, $model)
    {
        return $this->_objectManager->create($model)->load($id);
    }

     /*
     * check for dimensions of pacakge entered
     * @param array
     * return bool
     */
    public function validPackages($packages) {
        foreach ($packages as $package) {
            if ($package['params']['length'] == "" ||
            $package['params']['width'] == "" ||
            $package['params']['height'] == "" ) {
                return false;
            }
            if ($package['params']['length'] == 0 ||
            $package['params']['width'] == 0 ||
            $package['params']['height'] == 0 ) {
                return false;
            }
        }
        return true;
    }

}
